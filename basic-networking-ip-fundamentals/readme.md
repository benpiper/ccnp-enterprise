# Start Here

## Router Configurations

[Starting device configurations](before)—Use if you're setting up your own lab (e.g. GNS3)

[CML lab configuration](before/cml)—If you're using Cisco Modeling Labs (CML), import the lab topology file

[Ending configurations](after)—Verify your configurations against these
